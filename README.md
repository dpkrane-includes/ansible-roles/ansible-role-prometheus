# Ansible Prometheus role.

Роль устанавливает последнюю версию Prometheus с GitHub.

Добавляются exporters, указанные в inventory.

Структура inventory для корректного формирования файла конфигурации:

    prometheus:
        hosts:
            prometheus0:
                ansible_host:
    cadvisors:
        hosts:
            cad:
                ansible_host:
                exporter_port: 9120
    nginx_exporters:
        hosts:
            nginx:
                ansible_host:
                exporter_port: 9110
    node_exporters:
        hosts:
            node:
                ansible_host:
                exporter_port: 9100
    blackbox_exporters:
        hosts:
            node:
                ansible_host:
                exporter_port: 9115
    exporters:
        children:
            cadvisors:
            nginx_exporters:
            node_exporters:
            blackbox_exporters:

  
  ## По умолчанию используются следующие переменные:
    prometheus_user: prometheus
    prometheus_group: prometheus
    prometheus_data_dir: /var/lib/prometheus
    prometheus_conf_dir: /etc/prometheus
    prometheus_rules_dir: /etc/prometheus/rules
    prometheus_rulesd_dir: /etc/prometheus/rules.d
    prometheus_files_dir: /etc/prometheus/files_sd
    prometheus_query_dir: /etc/prometheus/data
    prometheus_tmp_dir: /tmp
    prometheus_bin: /usr/local/bin/prometheus
    prometheus_pmtool_bin: /usr/local/bin/promtool
    prometheus_bind_addr: 0.0.0.0
    prometheus_bind_port: 9090
    prometheus_ext_url: ''

